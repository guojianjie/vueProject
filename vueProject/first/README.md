# first

> A Vue.js project

## Build Setup

``` bash
# 安装依赖
npm install

# 启动服务   浏览器中访问localhost:8080
npm run dev

# 构建打包
npm run build
```

For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).
